package org.tensorflow.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {

        button = (Button) findViewById(R.id.camera_button);

        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                Intent newIntent =
                        new Intent(MainActivity.this,DetectorActivity.class);
                startActivity(newIntent);

            }
        });
    }
}

